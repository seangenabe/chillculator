export default {
  theme: {
    screens: {
      sm: "600px",
      lg: "1024px"
    }
  },
  corePlugins: {
    preflight: false,
    // container: false,
    accessibility: false,
    appearance: false,
    // backgroundAttachment: false,
    // backgroundColor: false,
    // backgroundPosition: false,
    // backgroundRepeat: false,
    // backgroundSize: false,
    borderCollapse: false,
    // borderColor: false,
    // borderRadius: false,
    // borderStyle: false,
    // borderWidth: false,
    // cursor: false,
    // display: false,
    // flexDirection: false,
    // flexWrap: false,
    // alignItems: false,
    alignSelf: false,
    // justifyContent: false,
    // alignContent: false,
    // flex: false,
    // flexGrow: false,
    // flexShrink: false,
    order: false,
    float: false,
    // fontFamily: false,
    // fontWeight: false,
    // height: false,
    lineHeight: false,
    listStylePosition: false,
    listStyleType: false,
    // margin: false,
    // maxHeight: false,
    // maxWidth: false,
    // minHeight: false,
    // minWidth: false,
    objectFit: false,
    objectPosition: false,
    opacity: false,
    outline: false,
    // overflow: false,
    // padding: false,
    placeholderColor: false,
    pointerEvents: false,
    position: false,
    inset: false,
    resize: false,
    boxShadow: false,
    fill: false,
    stroke: false,
    tableLayout: false,
    // textAlign: false,
    // textColor: false,
    // fontSize: false,
    // fontStyle: false,
    // textTransform: false,
    // textDecoration: false,
    fontSmoothing: false,
    letterSpacing: false,
    userSelect: false,
    verticalAlign: false,
    visibility: false,
    whitespace: false,
    wordBreak: false,
    //width: false,
    zIndex: false
  },
  variants: {
    backgroundColor: false,
    backgroundAttachment: false,
    backgroundPosition: false,
    backgroundRepeat: false,
    backgroundSize: false,
    borderCollapse: false,
    cursor: false,
    textDecoration: false
  },
  plugins: [
    ({ addUtilities }) => {
      const utilities: Record<string, Record<string, string>> = {
        ".d-grid": {
          display: "grid"
        },
        ".justify-content-start": {
          "justify-content": "start"
        },
        ".justify-content-end": {
          "justify-content": "end"
        },
        ".text-0": {
          "font-size": "0"
        }
      }
      for (let i = 1; i <= 5; i++) {
        utilities[".gtc-" + i] = {
          "grid-template-columns": `repeat(${i}, 1fr)`
        }
        utilities[".gtr-" + i] = {
          "grid-template-rows": `repeat(${i}, 1fr)`
        }
      }
      addUtilities(utilities, {
        variants: ["responsive"]
      })
    }
  ]
}
