import webpack from "webpack"
import HtmlWebpackPlugin from "html-webpack-plugin"
import OfflinePlugin from "offline-plugin"
import TsconfigPathsPlugin from "tsconfig-paths-webpack-plugin"
import MiniCssExtractPlugin from "mini-css-extract-plugin"
import tailwindCss from "tailwindcss"
import tailwindConfig from "./tailwind.config"
import purgecss from "@fullhuman/postcss-purgecss"
import rxPaths from "rxjs/_esm2015/path-mapping"
import PwaManifest, { ManifestOptions } from "webpack-pwa-manifest"
import TerserPlugin from "terser-webpack-plugin"

const { NODE_ENV = "development" } = process.env
const isDev = NODE_ENV === "development"

const purgecssPlugin = purgecss({
  content: ["src/**/*.tsx"],
  defaultExtractor: (content: string) =>
    content.match(/[A-Za-z0-9-_:/]+/g) || [],
  whitelistPatterns: [/mdc/],
  whitelistPatternsChildren: [/mdc/]
})

const manifest: ManifestOptions = {
  name: "chillculator",
  short_name: "chillculator",
  start_url: "/",
  display: "standalone",
  background_color: "#4dd0e1",
  theme_color: "#4dd0e1",
  icons: [
    {
      src: `${__dirname}/dist/icon.png`,
      sizes: [16, 32, 64, 128, 192, 256, 512, 1024]
    }
  ]
}

const config: webpack.Configuration = {
  entry: "./src/index.tsx",
  output: {
    path: `${__dirname}/dist`,
    filename: "index.bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        options: {
          // load ts files from preact-material-component
          allowTsInNodeModules: true
        }
      },
      {
        test: /\.s?css$/i,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: isDev
            }
          },
          { loader: "css-loader", options: { importLoaders: 2 } },
          {
            loader: "postcss-loader",
            options: {
              plugins: [
                tailwindCss(tailwindConfig),
                ...(isDev ? [] : [purgecssPlugin])
              ]
            }
          }
          /* { loader: "sass-loader" } */
        ]
      }
    ]
  },
  resolve: {
    alias: rxPaths(),
    extensions: [".tsx", ".ts", ".js", ".ne"],
    plugins: [new TsconfigPathsPlugin({ configFile: "./src/tsconfig.json" })]
  },
  devtool: isDev ? "eval-source-map" : "source-map",
  plugins: [
    new HtmlWebpackPlugin({
      title: manifest.name,
      favicon: `${__dirname}/dist/icon16.png`
    }),
    new MiniCssExtractPlugin({
      filename: isDev ? "[name].css" : "[name].[hash].css"
    }),
    new PwaManifest(manifest),
    new OfflinePlugin({
      AppCache: false,
      externals: [
        "https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined",
        "https://fonts.gstatic.com/s/materialicons/v48/flUhRq6tzZclQEJ-Vdg-IuiaDsNcIhQ8tQ.woff2",
        "https://fonts.gstatic.com/s/materialiconsoutlined/v13/gok-H7zzDkdnRel8-DQ6KAXJ69wP1tGnf4ZGhUcel5euIg.woff2"
      ]
    })
  ],
  devServer: {
    contentBase: "static",
    hot: true,
    overlay: true
  },
  optimization: {
    minimize: !isDev,
    minimizer: [
      new TerserPlugin({
        extractComments: true,
        sourceMap: true,
        terserOptions: {
          ecma: 8
        }
      })
    ],
    nodeEnv: NODE_ENV
  },
  stats: { colors: true },
  node: {
    process: false,
    Buffer: false,
    __filename: false,
    __dirname: false
  }
}

export default config
