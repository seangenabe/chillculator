import sharp from "sharp"

const img = sharp(`${__dirname}/static/assets/icons/icon.svg`)

img
  .resize({ width: 1024, height: 1024 })
  .png()
  .toFile(`${__dirname}/dist/icon.png`)

img
  .resize({ width: 16, height: 16 })
  .png()
  .toFile(`${__dirname}/dist/icon16.png`)
