import { visitString, PARTIAL } from "chillcas/language/visitor"

export function calculate(input: string): string {
  try {
    // Treat empty string as partial input
    if (input === "") {
      return ""
    }
    // Parse input
    const visited = visitString(input)
    if (visited.result == null) {
      return "_reset"
    }
    // If partial input, blank the output
    if (visited.result == PARTIAL || visited.calculations === 0) {
      return ""
    }
    return `${visited.result}`.replace("-", "−")
  } catch (err) {
    // Parse error.
    return "_reset"
  }
}
