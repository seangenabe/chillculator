import { Subject } from "rxjs"

/**
 * Emit an object to this subject to focus the main text field.
 */
export const focusMainText = new Subject<void>()
