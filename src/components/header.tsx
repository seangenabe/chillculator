import { h } from "preact"
import { route } from "preact-router"
import TopAppBar from "preact-material-components/ts/TopAppBar"
import { MainDrawer } from "./main-drawer"
import { PureComponent } from "util/pure-component"

export class Header extends PureComponent<HeaderProps, HeaderState> {
  state: HeaderState = {
    isDrawerOpen: false
  }

  setIsDrawerOpen(isDrawerOpen: boolean) {
    this.setState(() => ({ isDrawerOpen }))
  }

  toggleDrawerOpen() {
    this.setState(({ isDrawerOpen }) => ({ isDrawerOpen: !isDrawerOpen }))
  }

  go(path: string) {
    route(path)
    this.setIsDrawerOpen(false)
  }

  render({ selectedRoute }, { isDrawerOpen }) {
    return (
      <div>
        <TopAppBar onNav={undefined as any} className="mdc-top-app-bar--fixed">
          <TopAppBar.Row>
            <TopAppBar.Section align-start>
              <TopAppBar.Icon
                onClick={() => this.setIsDrawerOpen(true)}
                className="cursor-pointer"
              >
                menu
              </TopAppBar.Icon>
              <TopAppBar.Title>Chillculator</TopAppBar.Title>
            </TopAppBar.Section>
          </TopAppBar.Row>
        </TopAppBar>
        <MainDrawer
          isOpen={isDrawerOpen}
          onOpenChanged={isOpen => this.setIsDrawerOpen(isOpen)}
          selectedRoute={selectedRoute}
          go={path => this.go(path)}
        />
      </div>
    )
  }
}

export interface HeaderProps {
  selectedRoute: string
}

export interface HeaderState {
  isDrawerOpen: boolean
}
