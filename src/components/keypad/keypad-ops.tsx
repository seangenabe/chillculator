import { h, JSX, ComponentChildren } from "preact"
import { KeypadButton } from "./keypad-button"
import { pure } from "util/pure-component"
import "./keypad-ops.css"
import { Observable } from "rxjs"

const keys: (string | { symbol: string; render: () => ComponentChildren })[] = [
  {
    symbol: "C",
    render: () => <i className="material-icons-outlined">backspace</i>
  },
  "÷",
  "×",
  "−",
  "+",
  "="
]

export const KeypadOps = pure(function _KeypadOps(props: KeypadOpsProps) {
  const { onPress = () => {}, resizeObservable } = props
  return (
    <div {...props} className={`d-grid keypad-ops ${props.className || ""}`}>
      {keys.map(key => {
        let symbol: string
        let children: ComponentChildren
        if (typeof key === "string") {
          symbol = key
          children = key
        } else {
          symbol = key.symbol
          children = key.render()
        }
        return (
          <KeypadButton
            onPress={onPress}
            symbol={symbol}
            className={`keypad-ops__symbol-${symbol}`}
            buttonClassName="mdc-theme--text-primary-on-background"
            resizeObservable={resizeObservable}
          >
            {children}
          </KeypadButton>
        )
      })}
    </div>
  )
})

export interface KeypadOpsProps extends JSX.HTMLAttributes {
  onPress?(symbol: string, e: MouseEvent): void
  resizeObservable?: Observable<number | "auto">
}
