import { h, JSX, ComponentChildren } from "preact"
import { pure } from "util/pure-component"
import { KeypadButton } from "./keypad-button"
import "./keypad-advanced.css"
import { Observable } from "rxjs"
import { simpleTrig } from "language/functions"

const keys: ({
  symbol: string
  label: string | (() => ComponentChildren)
})[] = [
  ...simpleTrig.map(s => ({ symbol: s + "(", label: s })),
  { symbol: "√(", label: "√" },
  { symbol: "^", label: "x^y" },
  ...["E", "π", "e", "%"].map(s => ({ symbol: s, label: s }))
]

export const KeypadAdvanced = pure<KeypadAdvancedProps>(
  function _KeypadAdvanced(props) {
    const { onPress = () => {}, resizeObservable } = props
    return (
      <div
        {...props}
        className={`d-grid gtc-3 keypad-advanced ${props.className || ""}`}
        style={{ gridTemplateRows: "repeat(3, 1fr)" }}
      >
        {keys.map(({ symbol, label }) => (
          <KeypadButton
            onPress={onPress}
            symbol={symbol}
            buttonClassName="mdc-theme--text-primary-on-background"
            resizeObservable={resizeObservable}
          >
            {typeof label === "function" ? label() : label}
          </KeypadButton>
        ))}
      </div>
    )
  }
)

export interface KeypadAdvancedProps extends JSX.HTMLAttributes {
  onPress?(symbol: string): void
  resizeObservable?: Observable<number | "auto">
}
