import { h } from "preact"
import { pure } from "util/pure-component"
import { KeypadDigits } from "./keypad-digits"
import { KeypadOps } from "./keypad-ops"
import "./index.css"
import { KeypadAdvanced } from "./keypad-advanced"
import { Subject } from "rxjs"
import { SizeReactor } from "components/size-reactor"

export const Keypad = pure<KeypadProps>(function _Keypad({ onPress }) {
  const resize = new Subject<number | "auto">()
  return (
    <SizeReactor
      onResize={() => {
        resize.next(2140)
        resize.next("auto")
      }}
      className="keypad d-grid overflow-visible mx-auto py-4 h-full"
      style={{
        justifyItems: "stretch",
        alignItems: "stretch"
      }}
    >
      <KeypadDigits
        onPress={onPress}
        resizeObservable={resize.asObservable()}
      />
      <KeypadOps onPress={onPress} resizeObservable={resize.asObservable()} />
      <KeypadAdvanced
        onPress={onPress}
        resizeObservable={resize.asObservable()}
      />
    </SizeReactor>
  )
})

export interface KeypadProps {
  onPress(symbol: string): void
}
