import { h, JSX } from "preact"
import { KeypadButton } from "./keypad-button"
import { pure } from "util/pure-component"
import "./keypad-digits.css"
import { Observable } from "rxjs"

const symbols = ["7", "8", "9", "4", "5", "6", "1", "2", "3", "0", ".", "="]

export const KeypadDigits = pure<KeypadDigitsProps>(function _KeypadDigits(
  props: KeypadDigitsProps
) {
  const { onPress = () => {}, resizeObservable } = props
  return (
    <div
      className="d-grid w-full keypad-digits"
      {...props}
      style={{ overflow: "visible" }}
    >
      {symbols.map(symbol => (
        <KeypadButton
          onPress={onPress}
          className={symbol === "=" ? "keypad-digits__symbol-=" : ""}
          buttonClassName="mdc-theme--on-primary"
          symbol={symbol}
          resizeObservable={resizeObservable}
        />
      ))}
    </div>
  )
})

export interface KeypadDigitsProps extends JSX.HTMLAttributes {
  onPress?(symbol: string, e: MouseEvent): void
  resizeObservable?: Observable<number | "auto">
}
