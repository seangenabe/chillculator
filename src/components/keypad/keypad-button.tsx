import { h, JSX } from "preact"
import { MDCRipple } from "@material/ripple"
import { PureComponent } from "util/pure-component"
import { Subscription, Observable, EMPTY } from "rxjs"

const OVERSIZE = 0.2

export class KeypadButton extends PureComponent<KeypadButtonProps> {
  private sub = Subscription.EMPTY
  private containerElement: HTMLDivElement | null = null
  private setContainer(container: HTMLDivElement | null) {
    this.containerElement = container
    this.resizeContainer2("auto")
  }
  private container2Element: HTMLDivElement | null = null
  private setContainer2(container2: HTMLDivElement | null) {
    this.container2Element = container2
  }
  private buttonElement: HTMLButtonElement | null = null
  private setButton(button: HTMLButtonElement | null) {
    this.buttonElement = button
    this.updateMdc()
  }

  updateMdc() {
    if (!this.buttonElement) return
    const ripple = new MDCRipple(this.buttonElement)
    ripple.unbounded = true
  }

  async resizeContainer2(basis: number | "auto") {
    const container = this.containerElement
    if (!container) return
    let container2 = this.container2Element
    if (!container2) return

    if (basis === "auto") {
      // Fit inside container with 1:1 aspect ratio
      let size: number = Math.min(container.clientWidth, container.clientHeight)
      container2.style.flexGrow = size === 0 ? "1" : null
      if (container.clientWidth > container.clientHeight) {
        container.style.flexDirection = "row"
      } else {
        container.style.flexDirection = "column"
      }

      container2.style.flexBasis = `${size}px`
    } else {
      container2.style.flexGrow = "1"
      container2.style.flexBasis = `${basis}px`
    }
  }

  componentDidMount() {
    this.resizeContainer2("auto")
  }

  render(props: KeypadButtonProps) {
    const {
      onPress = () => {},
      symbol,
      resizeObservable = EMPTY,
      buttonClassName = ""
    } = props
    let { children } = props
    if (Array.isArray(children) && children.length === 0) {
      children = symbol
    }
    this.sub.unsubscribe()
    this.sub = resizeObservable.subscribe(basis => this.resizeContainer2(basis))

    return (
      <div
        ref={el => this.setContainer(el)}
        {...props}
        className={`flex justify-center items-stretch w-full h-full cursor-pointer ${props.className ||
          ""}`}
      >
        <div ref={el => this.setContainer2(el)}>
          <div
            ref={b => this.setButton(b)}
            className={`mdc-ripple-surface rounded-full flex flex-column items-center justify-center text-2xl position-relative ${buttonClassName}`}
            onClick={e => onPress(symbol, e)}
            tabIndex={-1}
            style={{
              height: `${(OVERSIZE + 1) * 100}%`,
              width: `${(OVERSIZE + 1) * 100}%`,
              marginLeft: `${OVERSIZE * -50}%`,
              marginTop: `${OVERSIZE * -50}%`
            }}
          >
            <div className="flex items-center justify-center">{children}</div>
          </div>
        </div>
      </div>
    )
  }
}

export interface KeypadButtonProps extends JSX.HTMLAttributes {
  symbol: string
  onPress?(symbol: string, e: MouseEvent): void
  resizeObservable?: Observable<number | "auto">
  buttonClassName?: string
}
