import "./app.css"
import { h, Component } from "preact"
import { Header } from "components/header"
import { Tape } from "components/tape"
import Helmet from "preact-helmet"
import { Subject } from "rxjs"
import { Keypad } from "components/keypad"

export class App extends Component<{}, AppState> {
  private inputSymbolSource = new Subject<string>()

  componentDidMount() {
    // Add classes to body element.
    "p-0 m-0 h-screen flex flex-col"
      .split(" ")
      .forEach(c => document.body.classList.add(c))
  }

  render() {
    return (
      <div className="flex flex-col h-full">
        <Helmet
          meta={[
            { name: "viewport", content: "initial-scale=1,maximum-scale=1" },
            { name: "mobile-web-app-capable", content: "yes" },
            { name: "apple-mobile-web-app-capable", content: "yes" }
          ]}
          link={[
            {
              href:
                "https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined",
              rel: "stylesheet"
            }
          ]}
        />
        <Header selectedRoute="/" />
        <main
          className="d-grid h-full"
          style={{ gridTemplateRows: "auto auto 1fr" }}
        >
          <div className="mdc-top-app-bar--fixed-adjust"></div>
          <div>
            <Tape inputSymbolSource={this.inputSymbolSource.asObservable()} />
            {/* <MainText value={input} onChange={s => this.calculate(s)} />*/}
          </div>
          <div className="container mx-auto overflow-hidden">
            <Keypad onPress={symbol => this.inputSymbol(symbol)} />
          </div>
        </main>
      </div>
    )
  }

  inputSymbol(symbol: string) {
    this.inputSymbolSource.next(symbol)
  }
}

export interface AppState {}
