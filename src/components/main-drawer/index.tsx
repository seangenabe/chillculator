import List from "preact-material-components/ts/List"
import Drawer from "preact-material-components/ts/Drawer"
import { h, FunctionalComponent } from "preact"

const MainDrawer: FunctionalComponent<MainDrawerProps> = function MainDrawer({
  isOpen,
  selectedRoute,
  go,
  onOpenChanged
}: MainDrawerProps) {
  const drawerItems = [
    { route: "/", graphic: "home", label: "Basic Calculator" }
  ].map(({ route, graphic, label }) => (
    <MainDrawerItem
      route={route}
      graphic={graphic}
      selectedRoute={selectedRoute}
      go={go}
    >
      {label}
    </MainDrawerItem>
  ))
  return (
    <Drawer
      modal
      open={isOpen}
      onOpen={() => onOpenChanged(true)}
      onClose={() => onOpenChanged(false)}
    >
      <Drawer.DrawerContent>{drawerItems}</Drawer.DrawerContent>
    </Drawer>
  )
}

export function MainDrawerItem({
  selectedRoute,
  route,
  graphic,
  go,
  children
}: {
  selectedRoute: string
  route: string
  graphic: string
  go(path: string): void
  children: string
}) {
  return (
    <List.LinkItem
      className="cursor-pointer"
      selected={selectedRoute === route}
      onClick={() => go(route)}
      tabindex={0}
    >
      <List.ItemGraphic>{graphic}</List.ItemGraphic>
      {children}
    </List.LinkItem>
  )
}

export { MainDrawer }

export interface MainDrawerProps {
  isOpen: boolean
  selectedRoute: string
  onOpenChanged(isOpen: boolean): void
  go(path: string): void
}
