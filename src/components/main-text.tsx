import { h, Component } from "preact"
import { TextField } from "preact-material-components/ts/TextField"
import { focusMainText } from "global-events"
import { Subscription } from "rxjs"

export class MainTextMutating extends Component<MainTextProps> {
  private text: HTMLInputElement | null = null
  private sub = Subscription.EMPTY

  private setText(el: HTMLInputElement | null ) {
    if (this.text) {
      this.sub.unsubscribe()
    }
    this.text = el
    this.sub = focusMainText.subscribe()
  }

  componentWillUnmount() {
    this.sub.unsubscribe()
  }

  render() {
    return (
      <TextField
        ref={el => this.setText(el)}
        onInput={e =>
          (this.props.onChange || (() => {}))(
            (e!.target! as HTMLInputElement).value
          )
        }
        onBlur={e => e.preventDefault()}
        value={this.props.value}
        fullwidth
      />
    )
  }
}

export class MainTextNonMutating extends Component<MainTextProps> {
  base: any = undefined

  shouldComponentUpdate() {
    return false
  }

  componentDidMount() {}

  componentWillUnmount() {}

  render() {
    return (
      <TextField
        onInput={e =>
          (this.props.onChange || (() => {}))(
            (e!.target! as HTMLInputElement).value
          )
        }
        onBlur={e => e.preventDefault()}
        value={this.props.value}
        fullwidth
      />
    )
  }
}

export class MainText extends MainTextMutating {}

export interface MainTextProps {
  onChange?(value: string): void
  value?: string
}
