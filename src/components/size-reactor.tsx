import { h, Component, JSX } from "preact"
import { Subscription, fromEvent } from "rxjs"

declare var ResizeObserver: any

export class SizeReactor extends Component<SizeReactorProps> {
  el: HTMLDivElement | undefined
  private sub = Subscription.EMPTY
  private setEl(el: HTMLDivElement | null) {
    if (el == null) return
    const { onResize = () => {} } = this.props
    this.el = el
    this.sub.unsubscribe()
    if (ResizeObserver) {
      const r = new ResizeObserver(() => onResize(el))
      r.observe(el)
      this.sub = new Subscription(() => r.unobserve(el))
    } else {
      this.sub = fromEvent(window, "resize").subscribe(() => onResize(el))
    }
    onResize(el)
  }

  render(props: SizeReactorProps) {
    return <div ref={el => this.setEl(el)} {...props} />
  }
}

export interface SizeReactorProps extends JSX.HTMLAttributes {
  onResize?(el: HTMLDivElement): void
}
