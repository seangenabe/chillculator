import { h, Component, createRef } from "preact"
import nanoid from "nanoid"
import deepEqual from "fast-deep-equal"
import { shallowEqual } from "util/shallow-equal"
import { Observable, Subscription, EMPTY } from "rxjs"
import "./tape.css"
import { calculate } from "language/calculate"
import { replaceTextInRange } from "util/range-replacement"
import { allFunctions } from "language/functions"

export class Tape extends Component<TapeProps, TapeState> {
  private propsSub = Subscription.EMPTY
  private root = createRef<HTMLDivElement>()
  private inputSelectionState: SelectionState = {
    value: "",
    selectionStart: null,
    selectionEnd: null,
    selectionDirection: null
  }

  state: TapeState = {
    oldEntries: [],
    inputEntry: createTapeEntry(),
    outputEntry: createTapeEntry(),
    extraEntry: createTapeEntry(),
    isMobile: false,
    highEnough: true
  }

  private heightQuery: MediaQueryList

  setInputOutputValue(inputValue: string, outputValue: string) {
    this.setState(({ inputEntry, outputEntry }) => ({
      inputEntry: { id: inputEntry.id, value: inputValue },
      outputEntry: { id: outputEntry.id, value: outputValue }
    }))
  }

  setInputValue(inputValue: string) {
    this.setState(({ inputEntry }) => ({
      inputEntry: { id: inputEntry.id, value: inputValue }
    }))
  }

  focus() {
    const el = this.getInputTextField()
    el && el.focus()
  }

  getInputTextField() {
    const root = this.root.current
    if (!root) {
      return
    }
    const [el] = root.getElementsByClassName("tape__input")
    if (!el) {
      return
    }
    return el as HTMLInputElement
  }

  calculate() {
    const el = this.getInputTextField()
    if (!el) {
      return
    }

    const replacementRules = [
      { regex: /^\s+/, replacement: "" }, // Whitespace
      { regex: /^\*/, replacement: "×" }, // Multiplication sign
      { regex: /^\//, replacement: "÷" }, // Division sign
      { regex: /^-/, replacement: "−" } // Minus sign
    ]

    for (let i = 0; i < el.value.length; i++) {
      for (let rule of replacementRules) {
        const result = el.value.substring(i).match(rule.regex)
        if (result) {
          const {
            value,
            selectionStart,
            selectionEnd,
            selectionDirection
          } = getSelectionState(el)
          if (selectionStart == null || selectionEnd == null) {
            el.setRangeText(
              rule.replacement,
              i,
              i + result[0].length,
              "preserve"
            )
          } else {
            let s = { value, selectionStart, selectionEnd }
            replaceTextInRange(s, i, i + result[0].length, rule.replacement)
            setSelectionState(el, { ...s, selectionDirection })
          }
          continue
        }
      }
    }

    const inputValue = el.value
    const inputSelectionState = getSelectionState(el)

    // Try calculate state
    const output = calculate(inputValue)
    if (output === "_reset") {
      // Revert
      setSelectionState(el, this.inputSelectionState)
    } else {
      // Save selection state
      this.inputSelectionState = inputSelectionState
      // Change input and output
      this.setInputOutputValue(inputSelectionState.value, `${output}`)
    }
  }

  symbolInputted(symbol: string) {
    const el = this.getInputTextField()
    if (!el) {
      return
    }
    const { selectionStart, selectionEnd } = el
    if (selectionStart == null || selectionEnd == null) {
      return
    }
    if (symbol === "C") {
      if (selectionStart !== selectionEnd) {
        // Delete current selection.
        el.setRangeText("")
      } else if (selectionStart === 0) {
        // Can't backspace past first character
        return
      } else {
        // Batch delete functions
        const { value } = el
        const textBeforeStart = value.substring(0, selectionStart)
        let deleted = false
        for (let functionName of allFunctions) {
          if (textBeforeStart.endsWith(functionName + "(")) {
            el.setRangeText(
              "",
              selectionStart - functionName.length - 1,
              selectionStart
            )
            deleted = true
            break
          }
        }
        if (!deleted) {
          el.setRangeText("", selectionStart - 1, selectionStart)
        }
      }
    } else if (symbol === "=") {
      this.feed()
      el.focus()
      return
    } else {
      // Replace the current selection with the symbol.
      el.setRangeText(symbol, selectionStart, selectionEnd, "end")
    }
    this.calculate()
    el.focus()
  }

  /**
   * Insert the calculation into the feed history
   */
  feed() {
    const { outputEntry } = this.state
    if (outputEntry.value === "") {
      // Invalid calculation
      return
    }
    // Feed
    this.setState(
      ({ oldEntries, inputEntry, outputEntry, extraEntry }) => ({
        oldEntries: [...oldEntries, inputEntry],
        inputEntry: outputEntry,
        outputEntry: extraEntry,
        extraEntry: createTapeEntry()
      }),
      () => {
        const el = this.getInputTextField()
        if (el) {
          // Focus
          el.focus()
          // Move caret to end
          el.setSelectionRange(el.value.length, el.value.length)
        }
      }
    )
  }

  componentDidMount() {
    const isMobile = /android|ip(?:od|ad|hone)|opera mini/i.test(
      navigator.userAgent
    )
    this.heightQuery = matchMedia("(min-height: 440px)")
    this.setState(() => ({ isMobile, highEnough: this.heightQuery.matches }))
    this.heightQuery.addEventListener("change", () =>
      this.setState(() => ({ highEnough: this.heightQuery.matches }))
    )
  }

  shouldComponentUpdate(nextProps: TapeProps, nextState: TapeState) {
    if (!shallowEqual(this.props, nextProps)) {
      return true
    }
    if (!deepEqual(this.state, nextState)) {
      return true
    }
    return false
  }

  render(
    { inputSymbolSource = EMPTY }: TapeProps,
    {
      oldEntries,
      inputEntry,
      outputEntry,
      extraEntry,
      isMobile,
      highEnough
    }: TapeState
  ) {
    this.propsSub.unsubscribe()
    this.propsSub = inputSymbolSource.subscribe(symbol =>
      this.symbolInputted(symbol)
    )

    const allEntries: {
      entry: TapeEntry
      type: "old" | "input" | "output" | "extra"
    }[] = [
      ...oldEntries.map(entry => ({ entry, type: "old" as const })),
      { entry: inputEntry, type: "input" as const },
      { entry: outputEntry, type: "output" as const },
      { entry: extraEntry, type: "extra" as const }
    ]
    return (
      <div
        className="flex flex-col py-3"
        onInput={e => this.calculate()}
        ref={this.root}
      >
        {allEntries.map(({ entry, type }) => {
          let className: string
          if (type === "output") {
            className = "text-3xl"
          } else if (type === "input") {
            className = "text-4xl"
          } else {
            className = "text-0"
          }
          const isCollapsed =
            type === "old" ||
            type === "extra" ||
            (type === "output" && !highEnough)

          return (
            <div
              className={`mdc-text-field mdc-text-field--fullwidth mdc-text-field--no-label px-3 tape__entry ${
                isCollapsed ? "tape__entry--collapsed" : ""
              }`}
              key={entry.id}
            >
              <input
                type="text"
                className={`mdc-text-field__input border-none text-right text-blue-400 ${className} ${
                  type === "input" ? "tape__input" : ""
                }`}
                value={entry.value}
                autofocus={type === "input"}
                readOnly={isMobile || type !== "input"}
              />
            </div>
          )
        })}
      </div>
    )
  }
}

function getSelectionState(input: HTMLInputElement): SelectionState {
  const { selectionStart, selectionEnd, selectionDirection, value } = input
  return { selectionStart, selectionEnd, selectionDirection, value }
}

function setSelectionState(
  input: HTMLInputElement,
  selectionState: SelectionState
): void {
  const {
    selectionStart,
    selectionEnd,
    selectionDirection,
    value
  } = selectionState
  input.value = value
  input.selectionStart = selectionStart
  input.selectionEnd = selectionEnd
  input.selectionDirection = selectionDirection
}

function createTapeEntry(): TapeEntry {
  return { id: nanoid(), value: "" }
}

interface SelectionState {
  value: string
  selectionStart: number | null
  selectionEnd: number | null
  selectionDirection: HTMLInputElement["selectionDirection"]
}

export interface TapeProps {
  inputSymbolSource?: Observable<string>
}

export interface TapeState {
  oldEntries: TapeEntry[]
  inputEntry: TapeEntry
  outputEntry: TapeEntry
  /** Extra entry to balance layout */
  extraEntry: TapeEntry
  isMobile: boolean
  highEnough: boolean
}

export interface TapeEntry {
  id: string
  value: string
}
