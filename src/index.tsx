import { App } from "components/app"
import { h, render } from "preact"
import OfflinePluginRuntime from "offline-plugin/runtime"

if (process.env.NODE_ENV === "production") {
  OfflinePluginRuntime.install()
}

let root: Element | undefined = undefined

function init() {
  root = render(<App />, document.body, root)
}

if (module.hot) {
  module.hot.accept("./components/app", () => requestAnimationFrame(init))
  require("preact/debug")
}

init()
