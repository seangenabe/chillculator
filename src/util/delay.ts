export function delay(ms: number) {
  return new Promise(r => setTimeout(r, ms))
}

export function raf() {
  return new Promise<number>(r => requestAnimationFrame(r))
}
