export interface TextSelectionState {
  value: string
  selectionStart: number
  selectionEnd: number
}

export function replaceTextInRange(
  state: TextSelectionState,
  oldStart: number,
  oldEnd: number,
  newString: string
) {
  const oldLength = oldEnd - oldStart
  const { selectionStart, selectionEnd } = state
  const newEnd = oldStart + newString.length
  const newLength = newString.length

  // Concat new value
  state.value =
    state.value.substring(0, oldStart) +
    newString +
    state.value.substring(oldEnd)

  // Set start of selection
  if (selectionStart <= oldStart) {
  } else if (selectionStart <= oldEnd) {
    state.selectionStart = Math.min(selectionStart, newEnd)
  } else {
    state.selectionStart = selectionStart - oldLength + newLength
  }

  // Set end of selection
  if (selectionEnd <= oldStart) {
  } else if (selectionEnd <= oldEnd) {
    state.selectionEnd = Math.min(selectionEnd, newEnd)
  } else {
    state.selectionEnd = selectionEnd - oldLength + newLength
  }
  return state
}
