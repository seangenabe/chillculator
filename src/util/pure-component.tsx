import { Component, FunctionalComponent } from "preact"
import { shallowEqual } from "./shallow-equal"

export abstract class PureComponent<
  P extends {} = {},
  S extends {} = {}
> extends Component<P, S> {
  shouldComponentUpdate(props: P, state: S) {
    return !(shallowEqual(props, this.props) && shallowEqual(state, this.state))
  }
}

export function pure<P = {}>(
  component: FunctionalComponent<P>
): new () => PureComponent<P> {
  return class extends PureComponent<P> {
    render() {
      return component(this.props, this.context)
    }
  }
}
