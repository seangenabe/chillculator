export function shallowEqual<A extends {}, B extends {}>(a: A, b: B) {
  for (let key in a) {
    if (a[key] !== b[(key as unknown) as string]) return false
  }
  for (let key in b) {
    if (!(key in a)) return false
  }
  return true
}
