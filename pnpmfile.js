module.exports = {
  hooks: {
    readPackage(pkg) {
      if (pkg.name === "ts-loader") {
        pkg.dependencies.typescript = "^3.6.4"
      }
      return pkg
    }
  }
}
